export enum ICON_NAMES {
  HOME = 'pets',
  CHAT = 'md-chatbubble-outline',
  PROFILE = 'user',
  HEART = 'heart',
  CROSS = 'x',
  TINDER = 'tinder',
  STAR = 'star',
}

export enum ACTION_TYPES {
  GET_CATS = 'GET_CATS',
  GET_CATS_SUCCESS = 'GET_CATS_SUCCESS',
  GET_CATS_ERROR = 'GET_CATS_ERROR',
  SHOW_NEXT_CAT = 'SHOW_NEXT_CAT',
}

export enum SCREEN_NAMES {
  HOME = 'CatMatcher',
  CHAT = 'Chat',
  PROFILE = 'Profile',
}
