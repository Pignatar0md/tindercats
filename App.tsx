/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */
import 'react-native-gesture-handler';
import React from 'react';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {NavigationContainer} from '@react-navigation/native';
import {
  AnimatedTabBarNavigator,
  TabElementDisplayOptions,
} from 'react-native-animated-nav-tab-bar';
import CatMatcherScreen from './src/screens/CatMatcherScreen';
import ProfileScreen from './src/screens/ProfileScreen';
import ChatScreen from './src/screens/ChatScreen';
import {TabScrenOptions} from './interfaces';
import {renderTabIcon} from './src/utils';
import {ICON_NAMES, SCREEN_NAMES} from './Enums';
import {palette} from './src/constants';
import {CatsContextProvider} from './src/state/context/CatsContext';

function App(): JSX.Element {
  const Tabs = AnimatedTabBarNavigator();

  return (
    <SafeAreaProvider>
      <CatsContextProvider>
        <NavigationContainer>
          <Tabs.Navigator
            appearance={{
              floating: true,
              shadow: true,
              whenActiveShow: TabElementDisplayOptions.ICON_ONLY,
            }}
            tabBarOptions={{
              activeTintColor: palette.fuchsia,
              activeBackgroundColor: palette.white,
            }}>
            <Tabs.Screen
              name={SCREEN_NAMES.HOME}
              component={CatMatcherScreen}
              options={{
                tabBarIcon: (options: TabScrenOptions) =>
                  renderTabIcon(ICON_NAMES.HOME, options),
              }}
            />
            <Tabs.Screen
              name={SCREEN_NAMES.CHAT}
              component={ChatScreen}
              options={{
                tabBarIcon: (options: TabScrenOptions) =>
                  renderTabIcon(ICON_NAMES.CHAT, options),
              }}
            />
            <Tabs.Screen
              name={SCREEN_NAMES.PROFILE}
              component={ProfileScreen}
              options={{
                tabBarIcon: (options: TabScrenOptions) =>
                  renderTabIcon(ICON_NAMES.PROFILE, options),
              }}
            />
          </Tabs.Navigator>
        </NavigationContainer>
      </CatsContextProvider>
    </SafeAreaProvider>
  );
}

export default App;
