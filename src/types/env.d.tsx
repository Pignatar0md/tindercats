declare module '@env' {
  export const BASE_URI: string;
  export const API_KEY: string;
}
