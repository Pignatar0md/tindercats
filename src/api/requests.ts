import axios, {AxiosRequestConfig, AxiosResponse} from 'axios';
import {API_KEY, BASE_URI} from '@env';
import {IMAGES_URL, VOTES_URL} from '../constants';

export const getCatInformation = async () => {
  const options: AxiosRequestConfig = {
    headers: {
      'content-type': 'application/json; charset=utf-8',
    },
    url: `${IMAGES_URL}&api_key=${API_KEY}`,
  };

  const response: AxiosResponse = await axios.get(
    `${BASE_URI}${IMAGES_URL}&api_key=${API_KEY}&has_breeds=1`,
    options,
  );
  return response.data;
};

export const sendCatVote = async ({
  imageId,
  vote,
}: {
  imageId: string;
  vote: number;
}) => {
  const options: AxiosRequestConfig = {
    method: 'post',
    url: `${BASE_URI}${VOTES_URL}/`,
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
      'x-api-key': API_KEY,
    },
    data: JSON.stringify({
      image_id: imageId,
      value: vote,
    }),
  };
  try {
    await axios(options);
  } catch (error: any) {
    return error.message;
  }
};
