import React from 'react';
import {StyleProp, Text} from 'react-native';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import Ionicon from 'react-native-vector-icons/Ionicons';
import FeatherIcon from 'react-native-vector-icons/Feather';
import Fontistoicon from 'react-native-vector-icons/Fontisto';
import {TabScrenOptions} from '../interfaces';
import {ICON_NAMES} from '../Enums';

export const renderTabIcon = (name: string, options: TabScrenOptions) => {
  MaterialIcon.loadFont().catch(error => {
    console.info(error);
  });
  Ionicon.loadFont().catch(error => {
    console.info(error);
  });
  FeatherIcon.loadFont().catch(error => {
    console.info(error);
  });

  const {size, color, focused} = options;
  const props = {
    name,
    size: size ?? 24,
    color: focused ? color : '#222222',
    focused,
  };
  const chatOrProfile =
    name === ICON_NAMES.CHAT ? (
      <Ionicon {...props} />
    ) : (
      <FeatherIcon {...props} />
    );
  return name === ICON_NAMES.HOME ? <MaterialIcon {...props} /> : chatOrProfile;
};

export const renderIcon = (
  wrapperStyle: StyleProp<{}>,
  iconName: ICON_NAMES,
  iconSize: number,
  color: string,
) => {
  Fontistoicon.loadFont().catch(error => {
    console.info(error);
  });
  return (
    <Text style={wrapperStyle}>
      <Fontistoicon name={iconName} size={iconSize} color={color} />
    </Text>
  );
};
