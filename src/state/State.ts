import {CatsState} from './interfaces';

export const initialState: CatsState = {
  loading: false,
  errorMessage: '',
  cats: [],
  catToShow: 0,
};
