/* eslint-disable @typescript-eslint/no-unused-vars */
import React, {createContext, ReactNode, useReducer} from 'react';
import {ACTION_TYPES} from '../../../Enums';
import {ActionTypes, Cat, CatsState} from '../interfaces';
import {initialState} from '../State';

const catsReducer = (state: CatsState, action: ActionTypes): CatsState => {
  switch (action.type) {
    case ACTION_TYPES.GET_CATS:
      return {...state, loading: true, errorMessage: ''};
    case ACTION_TYPES.GET_CATS_SUCCESS:
      return {
        ...state,
        loading: false,
        errorMessage: '',
        cats: action.payload as Cat[],
      };
    case ACTION_TYPES.SHOW_NEXT_CAT:
      return {...state, catToShow: action.payload as number};
    case ACTION_TYPES.GET_CATS_ERROR:
      return {
        ...state,
        loading: true,
        errorMessage: typeof action.payload === 'string' ? action.payload : '',
      };

    default:
      return state;
  }
};

export const CatContext = createContext({
  state: initialState,
  getCatInfo: () => {},
  getCatInfoSuccess: (catInfo: Cat[]) => {},
  getCatInfoError: (a: string) => {},
  showNextCat: (a: number) => {},
});

export function CatsContextProvider({children}: {children: ReactNode}) {
  const [state, dispatch] = useReducer(catsReducer, initialState as any);

  function getCatInfo() {
    dispatch({type: ACTION_TYPES.GET_CATS});
  }

  function getCatInfoSuccess(catInfo: Cat[]) {
    dispatch({type: ACTION_TYPES.GET_CATS_SUCCESS, payload: catInfo});
  }

  function getCatInfoError(message: string) {
    dispatch({type: ACTION_TYPES.GET_CATS_SUCCESS, payload: message});
  }

  function showNextCat(catNumber: number) {
    dispatch({type: ACTION_TYPES.SHOW_NEXT_CAT, payload: catNumber});
  }

  return (
    <CatContext.Provider
      value={{
        state,
        getCatInfo,
        getCatInfoSuccess,
        getCatInfoError,
        showNextCat,
      }}>
      {children}
    </CatContext.Provider>
  );
}
