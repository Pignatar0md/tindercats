export interface Cat {
  breeds: any;
  url: string;
  id: string;
}

export interface CatsState {
  loading: boolean;
  // catInfo: Cat;
  catToShow: number;
  cats: Cat[];
  errorMessage: string;
}

type GetCatInfoSuccess = {type: string; payload?: Cat[]};
type GetCatInfoError = {type: string; payload?: string};
type ShowNextCat = {type: string; payload: number};

export type ActionTypes = GetCatInfoError | GetCatInfoSuccess | ShowNextCat;
