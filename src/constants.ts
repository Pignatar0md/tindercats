export const palette = {
  fuchsia: '#EC537E',
  white: '#FFFFFF',
  green: '#6BD88E',
  red: '#E16359',
  black: '#000000',
  darkGrey: '#BFBFC0',
  lightGrey: '#E3E3E4',
  darkOrange: '#FF9900',
};

export const IMAGES_URL = '/images/search?limit=10';
export const VOTES_URL = '/votes';
