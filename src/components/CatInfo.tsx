import React, {FC, ReactElement} from 'react';
import {StyleSheet, View, Text} from 'react-native';
import {palette} from '../constants';
import {Cat} from '../state/interfaces';

const CatInfo: FC<{cat: Cat}> = ({cat}): ReactElement => {
  const {container, catAgeNationality, catName} = styles;
  return (
    <View style={container}>
      <View style={catAgeNationality}>
        <Text style={catName}>{cat?.breeds[0]?.name ?? ''}</Text>
        <Text>{cat?.breeds[0]?.life_span ?? ''}</Text>
      </View>
      <Text>{cat?.breeds[0]?.origin ?? ''}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 65,
    alignItems: 'stretch',
    width: 260,
    position: 'absolute',
    bottom: 0,
    backgroundColor: palette.white,
    justifyContent: 'center',
    paddingVertical: 5,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,
    paddingHorizontal: 15,
  },
  catAgeNationality: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  catName: {
    fontSize: 16,
    fontWeight: '700',
    lineHeight: 20,
  },
});

export default CatInfo;
