import React from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import Ionicon from 'react-native-vector-icons/Ionicons';
import Feathericon from 'react-native-vector-icons/Feather';
import {ICON_NAMES} from '../../Enums';
import {palette} from '../constants';

const IconButton = ({
  color,
  name,
  onPress,
}: {
  color: string;
  name: string;
  onPress: () => void;
}) => {
  const iconProps = {name, size: 44, color};
  const icon =
    name === ICON_NAMES.CROSS ? (
      <Feathericon {...iconProps} />
    ) : (
      <Ionicon {...iconProps} />
    );

  return (
    <View style={styles.button}>
      <TouchableOpacity onPress={onPress}>{icon}</TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 60,
    width: 60,
    backgroundColor: palette.white,
    borderRadius: 50,
    shadowColor: palette.black,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
});

export default IconButton;
