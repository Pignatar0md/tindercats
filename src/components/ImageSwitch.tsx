import 'react-native-gesture-handler';
import React, {useState} from 'react';
import {StyleSheet} from 'react-native';
import Switch from 'react-native-switch-toggles';
import {palette} from '../constants';
import {ICON_NAMES} from '../../Enums';
import {renderIcon} from '../utils';

global.__reanimatedWorkletInit = () => {};

const ImageSwitch = () => {
  const {indicator, thumbIcon} = styles;
  const [isEnabled, setIsEnabled] = useState<boolean>(false);
  const {fuchsia, darkOrange, darkGrey, lightGrey} = palette;

  return (
    <Switch
      size={40}
      value={isEnabled}
      onChange={value => setIsEnabled(value)}
      activeTrackColor={lightGrey}
      inactiveTrackColor={lightGrey}
      renderInactiveThumbIcon={() =>
        renderIcon(thumbIcon, ICON_NAMES.TINDER, 18, fuchsia)
      }
      renderActiveThumbIcon={() =>
        renderIcon(thumbIcon, ICON_NAMES.STAR, 18, darkOrange)
      }
      renderOffIndicator={() =>
        renderIcon(indicator, ICON_NAMES.STAR, 18, darkGrey)
      }
      renderOnIndicator={() =>
        renderIcon(indicator, ICON_NAMES.TINDER, 18, palette.darkGrey)
      }
    />
  );
};

const styles = StyleSheet.create({
  indicator: {fontSize: 12, color: palette.black},
  thumbIcon: {fontSize: 12, color: palette.lightGrey},
});

export default ImageSwitch;
