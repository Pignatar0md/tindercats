import React, {FC, ReactElement} from 'react';
import {ActivityIndicator, StyleSheet, View} from 'react-native';
import {Image} from '@rneui/themed';
import {palette} from '../constants';

const CatImage: FC<{url: string}> = ({url}): ReactElement => {
  return (
    <View style={styles.container}>
      <Image
        source={{uri: url}}
        containerStyle={styles.item}
        PlaceholderContent={<ActivityIndicator />}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    shadowColor: palette.black,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    marginTop: 20,
    alignItems: 'center',
  },
  item: {
    borderRadius: 16,
    width: 343,
    height: 446,
  },
});

export default CatImage;
