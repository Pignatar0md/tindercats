import React, {FC, ReactElement} from 'react';
import {SafeAreaView, StyleSheet, Text} from 'react-native';
import {palette} from '../constants';

const ProfileScreen: FC = (): ReactElement => {
  return (
    <SafeAreaView style={styles.container}>
      <Text style={styles.textStyle}>03</Text>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: palette.white,
  },
  textStyle: {
    fontWeight: '700',
    fontSize: 126,
    lineHeight: 171.86,
    color: palette.darkGrey,
  },
});

export default ProfileScreen;
