import React, {FC, ReactElement, useContext, useEffect} from 'react';
import {StyleSheet, View, SafeAreaView, ActivityIndicator} from 'react-native';
import IconButton from '../components/IconButton';
import ImageSwitch from '../components/ImageSwitch';
import {ICON_NAMES} from '../../Enums';
import {palette} from '../constants';
import {CatContext} from '../state/context/CatsContext';
import {getCatInformation, sendCatVote} from '../api/requests';
import CatImage from '../components/CatImage';
import CatInfo from '../components/CatInfo';

const CatMatcherScreen: FC = (): ReactElement => {
  const {container, buttonsContainer} = styles;
  const {state, getCatInfo, getCatInfoSuccess, getCatInfoError, showNextCat} =
    useContext(CatContext);

  useEffect(() => {
    async function getCats() {
      getCatInfo();
      try {
        const result = await getCatInformation();
        getCatInfoSuccess(result);
      } catch (error: any) {
        getCatInfoError(error.message);
      }
    }
    getCats();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const voteCat = async (vote: number) => {
    await sendCatVote({imageId: state.cats[state.catToShow].id, vote});
    showNextCat(state.catToShow + 1);
  };

  if (state.loading) {
    return <ActivityIndicator />;
  }

  return (
    <SafeAreaView style={container}>
      <ImageSwitch />
      <View style={styles.catInfoContainer}>
        <CatImage url={state?.cats[state?.catToShow]?.url} />
        <CatInfo cat={state?.cats[state?.catToShow]} />
      </View>
      <View style={buttonsContainer}>
        <IconButton
          name={ICON_NAMES.CROSS}
          color={palette.red}
          onPress={() => voteCat(-1)}
        />
        <IconButton
          name={ICON_NAMES.HEART}
          color={palette.green}
          onPress={() => voteCat(1)}
        />
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  catInfoContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    flex: 1,
    marginTop: 70,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: palette.white,
  },
  buttonsContainer: {
    height: 180,
    width: 300,
    justifyContent: 'space-around',
    alignItems: 'center',
    flexDirection: 'row',
    marginBottom: 54,
    marginHorizontal: 60,
    paddingHorizontal: 20,
  },
});

export default CatMatcherScreen;
