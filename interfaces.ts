export interface TabScrenOptions {
  focused: boolean;
  color: string;
  size: number;
}
